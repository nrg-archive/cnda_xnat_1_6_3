/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.io.File;
import java.util.Enumeration;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.launchers.AdemPipelineLauncher;
import org.nrg.pipeline.launchers.StdBuildLauncher;
import org.nrg.pipeline.utils.PipelineUtils;
import org.nrg.xdat.om.ArcPipelineparameterdata;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

public class PipelineScreen_ADEMBuild extends DefaultPipelineScreen  {

	String mprageScanType;
	String t1ScanType;
	String t2ScanType;
	String flairScanType;
	String dtiScanType;
	
	static Logger logger = Logger.getLogger(PipelineScreen_ADEMBuild.class);
	
	
	 public void preProcessing(RunData data, Context context)   {
		     super.preProcessing(data, context);
      	 }

	 
	 
	 public void finalProcessing(RunData data, Context context){
			XnatMrsessiondata mr = (XnatMrsessiondata) om;
	    	context.put("mr", mr); 
	    	try {
			    ArcPipelineparameterdata  mprageParam = 	getProjectPipelineSetting(AdemPipelineLauncher.MPRAGE_PARAM);
			    if (mprageParam != null) {
			       mprageScanType = mprageParam.getCsvvalues();
			       setHasDicomFiles(mr, mprageScanType, context);
			       setHasFreesurfer(mr,  context) ;
			    }

			     ArcPipelineparameterdata t1Param = 	 getProjectPipelineSetting(AdemPipelineLauncher.T1_PARAM);
				    if (t1Param != null)
				    	t1ScanType = t1Param.getCsvvalues();

			    ArcPipelineparameterdata t2Param = 	 getProjectPipelineSetting(AdemPipelineLauncher.T2_PARAM);
			    if (t2Param != null)
			    	t2ScanType = t2Param.getCsvvalues();

			    ArcPipelineparameterdata flairParam = 	 getProjectPipelineSetting(AdemPipelineLauncher.FLAIR_PARAM);
			    if (flairParam != null)
			    	flairScanType = flairParam.getCsvvalues();

			    
			    ArcPipelineparameterdata  dtiParam = 	 getProjectPipelineSetting(AdemPipelineLauncher.DTI_PARAM);
			    if (dtiParam != null)
			    	dtiScanType = dtiParam.getCsvvalues();
		     
	    	String selfStatus = WrkWorkflowdata.GetLatestWorkFlowStatusByPipeline(mr.getId(), XnatMrsessiondata.SCHEMA_ELEMENT_NAME, StdBuildLauncher.LOCATION+File.separator +StdBuildLauncher.NAME, mr.getProject(), TurbineUtils.getUser(data));
	    	if (selfStatus.equalsIgnoreCase(WrkWorkflowdata.COMPLETE)) {
	    		data.setMessage("This pipeline has already completed. Relaunching the pipeline may result in loss of processed files");
	    	}
	        	String pathToTarget = getTarget(); 
	        	context.put("target",pathToTarget);
	        	context.put("target_name",getTargetName(pathToTarget));
	        	context.put("projectSettings", projectParameters);
	        	LinkedHashMap<String,String> buildableScanTypes = new LinkedHashMap<String,String>();
	        	if (mprageScanType != null) 
	        		buildableScanTypes.put(AdemPipelineLauncher.MPRAGE, mprageScanType);
	        	if (t1ScanType != null) 
	        		buildableScanTypes.put(AdemPipelineLauncher.T1, t1ScanType);
	        	if (t2ScanType != null) 
	        		buildableScanTypes.put(AdemPipelineLauncher.T2,t2ScanType);
	        	if (flairScanType != null)  {
	        		buildableScanTypes.put(AdemPipelineLauncher.FLAIR, flairScanType);
	        	}
	        	if (dtiScanType != null)  {
	        		buildableScanTypes.put(AdemPipelineLauncher.DTI, dtiScanType);
	        	}
	        	
	        	context.put("buildableScanTypes", buildableScanTypes);

		  		 String cross_day_register = "0";
			  		ArcPipelineparameterdata parameter  = 	 getProjectPipelineSetting(PipelineUtils.CROSS_DAY_REGISTER);
			         if (parameter.getCsvvalues().equals("1")) {
			        	  context.put("cross_day_list", XnatMrsessiondata.GetAllPreviousImagingSessions(mr, mr.getProject(), TurbineUtils.getUser(data)));
			          }
		 }catch(Exception e) {
			 logger.error("Possibly the project wide pipeline has not been set", e);
			 e.printStackTrace();
		 }
	 }
	 
		protected String getTarget() {
			String rtn = null;
			Enumeration<String>  keysEnum = projectParameters.keys();
			while (keysEnum.hasMoreElements()) {
				String key = keysEnum.nextElement(); 
				if (key.equalsIgnoreCase("target" )) {	
					ArcPipelineparameterdata param = projectParameters.get(key);
					rtn = param.getCsvvalues();
					break;
				}
			}
			return rtn;
		}
		
		protected String getTargetName(String atlasPath) {
			String rtn = null; if (atlasPath == null) return rtn;
			rtn = atlasPath;
			int indexOfLastSlash = atlasPath.lastIndexOf(File.separator);
			if (indexOfLastSlash != -1) {
				String targetName = atlasPath.substring(indexOfLastSlash + 1);
				int indexOfDot = targetName.indexOf(".");
				if (indexOfDot != -1)
				   rtn = targetName.substring(0,indexOfDot);
				else
					rtn = targetName;
			}
			return rtn;
		}
	 
	
	 
	 
}
