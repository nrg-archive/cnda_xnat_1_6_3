/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.util.ArrayList;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.collections.ItemCollection;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.search.ItemSearch;

public class BuildScreen_PIBFSProcessing extends SecureReport {
	
	private ArrayList freesurferMrs;
	private ArrayList mrs;
	
    public void finalProcessing(RunData data, Context context)
    {
        XnatPetsessiondata pet = new XnatPetsessiondata(item);
        mrs = new ArrayList();
        freesurferMrs = new ArrayList();

        if (pet.getScans_scan() == null || pet.getScans_scan().size() == 0) {
            data.setMessage("No files uploaded for the session. Please upload files first");
            return;
        }
        
        if (pet.getReconstructions_reconstructedimage() == null || pet.getReconstructions_reconstructedimage().size() == 0 ) {
            data.setMessage("This session hasnt been processed yet. Please process the session first");
            return;
        }
        
        
        setMrItems(data,pet.getSubjectId());

        context.put("pet", pet);
        context.put("mrs", mrs);
        context.put("freesurfermrs", freesurferMrs);

    }
    
    private void setMrItems(RunData data, String search_value) {
        XnatMrsessiondata om = null;
        try {
            ItemSearch search = new ItemSearch();
            search.setUser(TurbineUtils.getUser(data));
            String elementName = "xnat:mrSessionData";
            SchemaElementI gwe = SchemaElement.GetElement(elementName);
            search.setElement(elementName);
            search.addCriteria("xnat:mrSessionData.subject_ID",search_value);
            boolean b = false;
            b= gwe.isPreLoad();
            search.setAllowMultiples(b);
            ItemCollection items = search.exec();
            ArrayList mrsByDate = items.getItems("xnat:mrSessionData.Date","DESC");
            if (mrsByDate.size() > 0)
            {
                for (int i =0; i <mrsByDate.size(); i++) {
                    ItemI item = (ItemI)mrsByDate.get(i);
                    om = (XnatMrsessiondata)BaseElement.GetGeneratedItem(item);
                    ArrayList aparcAssessors = om.getAssessors("fs:aparcRegionAnalysis");
                    ArrayList asegAssessors = om.getAssessors("fs:asegRegionAnalysis"); 
                    if (aparcAssessors.size() > 0 && asegAssessors.size() > 0 ) {
                    	//MR Session has Freesurfer data
                        freesurferMrs.add(om);
                    }
                    mrs.add(om);
                }
            }
        }catch(Exception e) {e.printStackTrace();}
    }

}
