/*
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 *
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.io.File;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.launchers.StdBuildLauncher;
import org.nrg.xdat.om.ArcPipelineparameterdata;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

public class PipelineScreen_fs_fsdata_relaunch extends DefaultPipelineScreen  {

	final String LOCATION = "build-tools";
	final String NAME =  "FreeSurfer_64bit_v5.xml";

	static Logger logger = Logger.getLogger(PipelineScreen_FS5_64.class);
	 public void finalProcessing(RunData data, Context context){
		 try {
		 }catch(Exception e) {
			 logger.error("Possibly the project wide pipeline has not been set", e);
			 e.printStackTrace();
		 }
	 }

}
