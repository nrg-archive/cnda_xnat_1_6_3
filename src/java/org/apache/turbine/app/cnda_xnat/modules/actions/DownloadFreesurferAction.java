/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.actions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatImageresource;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.base.BaseXnatExperimentdata.UnknownPrimaryProjectException;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFT;
import org.nrg.xft.utils.FileUtils;
import org.nrg.xft.utils.zip.TarUtils;
import org.nrg.xft.utils.zip.ZipI;
import org.nrg.xft.utils.zip.ZipUtils;

public class DownloadFreesurferAction extends SecureAction {

	static Logger logger = Logger.getLogger(DownloadFreesurferAction.class);
	
	public void doPerform(RunData data, Context context) throws Exception {
		  XnatMrsessiondata mr = null;
		  ItemI o = TurbineUtils.getDataItem(data);
		  if (o==null)o = TurbineUtils.GetItemBySearch(data,Boolean.TRUE);
		  if (o!=null)mr = (XnatMrsessiondata)BaseElement.GetGeneratedItem(o);
		  if (mr == null) {
			  logger.error("Couldnt load MR from RunData");
			  throw new NullPointerException();
		  }
	        try {
	            java.util.Date today = java.util.Calendar.getInstance(java.util.TimeZone.getDefault()).getTime();
	            String fileName=mr.getLabel() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".zip";
	            String contentType = "application/zip";
	            boolean tar = false;
				ZipI zip = null;

	            if (TurbineUtils.HasPassedParameter("download_type",data))   {
	                if (TurbineUtils.GetPassedParameter("download_type",data).equals("xar"))   {
	                    contentType = "application/xar";
	                    fileName=mr.getLabel() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".xar";
	                }else if (TurbineUtils.GetPassedParameter("download_type",data).equals("tar"))   {
	                    tar= true;
	                    contentType = "application/tar.gz";
	                    fileName=mr.getLabel() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".tar.gz";
	                }
	            }

	            ArrayList<File> files = null;
	            String freesurfer_path = data.getParameters().getString("freesurfer_path");
	       	            
	            String freesurfer_relative_path = data.getParameters().getString("freesurfer_relative_path");
	            if (freesurfer_relative_path != null) {
		            if (TurbineUtils.HasPassedParameter("download_file",data))   {
		              files = populateFileList(mr,data.getParameters().getString("download_file"), freesurfer_relative_path, false); 	
		            }
	            }else {
		            if (TurbineUtils.HasPassedParameter("download_file",data))   {
			              files = populateFileList(mr,data.getParameters().getString("download_file"), freesurfer_path, true); 	
			            }
	            }
	            
	            HttpServletResponse response= data.getResponse();
	    		response.setContentType(contentType);
				response.setHeader("Content-Disposition","inline;filename=" + fileName);
                OutputStream outStream = response.getOutputStream();
                if (tar) {
                	zip = new TarUtils();
                	 zip.setOutputStream(outStream,ZipOutputStream.DEFLATED);
                }
                else {
                	zip = new ZipUtils();
                	 zip.setOutputStream(outStream,ZipOutputStream.DEFLATED);
                }
                if (files != null && files.size() > 0) {
            		String rootPath = mr.getArchivePath() + mr.getLabel() + File.separator; 
                    if (freesurfer_relative_path != null) {
                    	writeToResponse( zip, files, mr.getLabel(), rootPath, freesurfer_relative_path, false); 
                    }else {
                    	writeToResponse( zip, files, mr.getLabel(), rootPath, freesurfer_path, true); 
                    }
                }
                zip.close();
	        }catch(Exception e) {
	        	logger.debug(e);
	        }
		  
	}
	
	private void writeToResponse(ZipI zip, ArrayList<File> files,  String sessionId, String rootPath, String freesurferRelativePath, boolean absolute) throws FileNotFoundException, IOException, URISyntaxException {
		if (!freesurferRelativePath.endsWith("/")) {
			freesurferRelativePath = freesurferRelativePath + "/";
		}
		File archiveRoot = new File(rootPath + freesurferRelativePath);
		File fspath = new File(freesurferRelativePath);
		URI baseUri = null;
		if (absolute) 
			baseUri = fspath.toURI();
		else
			baseUri = archiveRoot.toURI();
		for (File dirOrfile : files) {
    		if (dirOrfile.isDirectory())  {
    			write(zip,dirOrfile,sessionId,rootPath, freesurferRelativePath, absolute);
    		}else {
    			//String file = dirOrfile.toURI();
    			URI uri = dirOrfile.toURI();
    			URI relative = baseUri.relativize(uri);
    			if (absolute)
        			zip.write(relative.toString(),dirOrfile);
    			else	
    			zip.write(sessionId + File.separator +relative,dirOrfile);
    		}
    	}
	}
	
	private void write(ZipI zip, File dir, String path, String rootPath, String freesurferRelativePath, boolean absolute) throws FileNotFoundException, IOException, URISyntaxException{
		URI baseUri = null;
		File archiveRoot = new File(rootPath + freesurferRelativePath);
		File fspath = new File(freesurferRelativePath);
		if (absolute) 
			baseUri = fspath.toURI();
		else
			baseUri = archiveRoot.toURI();

		String dirName = dir.getName() + "/";
		 for(int i=0;i<dir.listFiles().length;i++) {
	            File child = dir.listFiles()[i];
	            if (child.isDirectory()) {
	                write(zip,child, path + "/" +  dir.getName() ,rootPath, freesurferRelativePath, absolute);
	            }else{
	    			//String file = child.getAbsolutePath();
	    			URI uri = child.toURI();
	    			URI relative = baseUri.relativize(uri);
	    			if (absolute)
		    			zip.write(relative.toString(),child);
	    			else	
	    				zip.write(path + File.separator +relative,child);
	            }
	        }
	}
	
	private ArrayList<File> populateFileList(XnatMrsessiondata im, String download_file_selection, String freesurferRelativePath, boolean absoluteFreesurferPath) {
		ArrayList<File> files = new ArrayList<File>();
		if (!freesurferRelativePath.endsWith("/")) {
			freesurferRelativePath = freesurferRelativePath + "/";
		}
		String rootPath = null;
		try {
			rootPath = im.getArchivePath() + im.getLabel() + "/";
		} catch (UnknownPrimaryProjectException e1) {
			logger.error("Could not get archive root path",e1);
		} 
		String sessionArchivePath = XFT.GetArchiveRootPath() ;
		
		if (download_file_selection.equals("ALLQC")) {
			//ArrayList<XnatAbstractresource> resources = im.getResourcesByLabel("FREESURFER_", false);
			//ArrayList<XnatAbstractresource> resources = getResourcesByLabel(im,"FREESURFER_", false);
			//HACK
			ArrayList<XnatAbstractresource> resources =  new ArrayList<XnatAbstractresource>();
			XnatImageresource brainmask =  new XnatImageresource();
			brainmask.setFormat("MGZ");
			if (absoluteFreesurferPath) 
				brainmask.setUri(freesurferRelativePath + im.getLabel()  + "/mri/brainmask.mgz");
			else
				brainmask.setUri(freesurferRelativePath  + "mri/brainmask.mgz");
			resources.add(brainmask);

			XnatImageresource aseg =  new XnatImageresource();
			aseg.setFormat("MGZ");
			if (absoluteFreesurferPath) 
				aseg.setUri(freesurferRelativePath + im.getLabel() +  "/mri/aseg.mgz");
			else
				aseg.setUri(freesurferRelativePath  + "mri/aseg.mgz");
			resources.add(aseg);
			
			XnatImageresource T1 =  new XnatImageresource();
			T1.setFormat("MGZ");
			if (absoluteFreesurferPath) 
				T1.setUri(freesurferRelativePath + im.getLabel() +  "/mri/T1.mgz");
			else	
				T1.setUri(freesurferRelativePath +  "mri/T1.mgz");
			resources.add(T1);
			
			XnatImageresource wm =  new XnatImageresource();
			wm.setFormat("MGZ");
			if (absoluteFreesurferPath) 
				wm.setUri(freesurferRelativePath + im.getLabel() +  "/mri/wm.mgz");
			else	
				wm.setUri(freesurferRelativePath +  "mri/wm.mgz");
			resources.add(wm);

			XnatImageresource orig =  new XnatImageresource();
			orig.setFormat("MGZ");
			if (absoluteFreesurferPath)
				orig.setUri(freesurferRelativePath + im.getLabel() +  "/mri/orig.mgz");
			else
				orig.setUri(freesurferRelativePath  + "mri/orig.mgz");
			resources.add(orig);

			XnatImageresource rh_white =  new XnatImageresource();
			rh_white.setFormat("SURF");
			if (absoluteFreesurferPath)
				rh_white.setUri(freesurferRelativePath + im.getLabel() +  "/surf/rh.white");
			else
				rh_white.setUri(freesurferRelativePath  + "surf/rh.white");
			resources.add(rh_white);

			XnatImageresource lh_white =  new XnatImageresource();
			lh_white.setFormat("SURF");
			if (absoluteFreesurferPath)
				lh_white.setUri(freesurferRelativePath + im.getLabel() +  "/surf/lh.white");
			else
				lh_white.setUri(freesurferRelativePath  + "surf/lh.white");
			resources.add(lh_white);

			
			for (XnatAbstractresource resource : resources) {
				XnatImageresource imageResource = (XnatImageresource) resource;
			    String uri = imageResource.getUri();
			    File f = new File(uri);
			    if (!FileUtils.IsAbsolutePath(uri)) 
			     f = new File(rootPath + uri);
			    if (f.exists()) files.add(f);
			    else {
			    	try {
						f = new File(im.getArchiveRootPath() + uri);
					} catch (UnknownPrimaryProjectException e) {
						logger.error("Could not get archive root path",e);
					}
			    	if (f.exists()) files.add(f);
			    	else {
				    	f = new File(sessionArchivePath + uri);
				    	if (f.exists()) files.add(f);
			    	}
			    }
			}
		}else if (download_file_selection.equals("ALL")) {
			File freesurferdir = new File(rootPath + freesurferRelativePath);
			if (absoluteFreesurferPath)
				freesurferdir = new File(freesurferRelativePath + im.getLabel());
			if (freesurferdir.exists() && freesurferdir.isDirectory()) {
				files.add(freesurferdir);
			}
		}else  {
			//ArrayList<XnatAbstractresource> resources = im.getResourcesByLabel(download_file_selection, true);
			ArrayList<XnatAbstractresource> resources = getResourcesByLabel(im,download_file_selection, true, freesurferRelativePath, absoluteFreesurferPath);
			for (XnatAbstractresource resource : resources) {
				XnatImageresource imageResource = (XnatImageresource) resource;
			    String uri = imageResource.getUri();
			    File f = new File(uri);
			    if (!FileUtils.IsAbsolutePath(uri)) 
			     f = new File(rootPath + uri);
			    if (f.exists()) files.add(f);
			    else {
			    	try {
						f = new File(im.getArchiveRootPath() + uri);
					} catch (UnknownPrimaryProjectException e) {
						logger.error("Could not get archive root path",e);
					}
			    	if (f.exists()) files.add(f); 
			    	else {
				    	f = new File(sessionArchivePath + uri);
				    	if (f.exists()) files.add(f);
			    	}
			    }
			}
		}
		return files;
	}
	
	
	private ArrayList<XnatAbstractresource> getResourcesByLabel(XnatMrsessiondata mr, String label, boolean exact, String relativepath, boolean absoluteFreesurferPath) {
		ArrayList<XnatAbstractresource> rtn = new ArrayList<XnatAbstractresource>();
		if (!relativepath.endsWith("/")) {
			relativepath = relativepath + "/";
		}
		
		if (label.equalsIgnoreCase("FREESURFER_WM")) {
			XnatImageresource wm =  new XnatImageresource();
			wm.setFormat("MGZ");
			if (absoluteFreesurferPath)
				wm.setUri(relativepath  + mr.getLabel()  + "/mri/wm.mgz");
			else
			wm.setUri(relativepath  + "mri/wm.mgz");
			rtn.add(wm);
		}else if (label.equalsIgnoreCase("FREESURFER_T1")) {
			XnatImageresource T1 =  new XnatImageresource();
			T1.setFormat("MGZ");
			if (absoluteFreesurferPath)
				T1.setUri(relativepath  + mr.getLabel()  + "/mri/T1.mgz");
			else
			T1.setUri(relativepath  + "mri/T1.mgz");
			rtn.add(T1);
		}else if (label.equalsIgnoreCase("FREESURFER_ORIG")) {
			XnatImageresource wm =  new XnatImageresource();
			wm.setFormat("MGZ");
			if (absoluteFreesurferPath)
				wm.setUri(relativepath  + mr.getLabel()  + "/mri/orig.mgz");
			else
			wm.setUri(relativepath  + "mri/orig.mgz");
			rtn.add(wm);
		}else if (label.equalsIgnoreCase("FREESURFER_BRAINMASK")) {
			XnatImageresource brainmask =  new XnatImageresource();
			brainmask.setFormat("MGZ");
			if (absoluteFreesurferPath)
				brainmask.setUri(relativepath + mr.getLabel() + "/mri/brainmask.mgz");
			else
			brainmask.setUri(relativepath  + "mri/brainmask.mgz");
			rtn.add(brainmask);

		}else if (label.equalsIgnoreCase("FREESURFER_RH_WHITE")) {
			XnatImageresource rh_white =  new XnatImageresource();
			rh_white.setFormat("SURF");
			if (absoluteFreesurferPath)
				rh_white.setUri(relativepath + mr.getLabel() + "/surf/rh.white");
			else
			rh_white.setUri(relativepath  + "surf/rh.white");
			rtn.add(rh_white);

		}else if (label.equalsIgnoreCase("FREESURFER_LH_WHITE")) {
			XnatImageresource lh_white =  new XnatImageresource();
			lh_white.setFormat("SURF");
			if (absoluteFreesurferPath)
				lh_white.setUri(relativepath + mr.getLabel() + "/surf/lh.white");
			else
			lh_white.setUri(relativepath  + "surf/lh.white");
			rtn.add(lh_white);

		}


		
		//If the label wasnt found....this is a hack
        //Create an imageResource with the uri
        if (rtn.size() == 0) {
			XnatImageresource img =  new XnatImageresource();
			img.setFormat("MGZ");
			img.setUri(label);
			rtn.add(img);
        }
		return rtn;
	}
	

	
	

}
