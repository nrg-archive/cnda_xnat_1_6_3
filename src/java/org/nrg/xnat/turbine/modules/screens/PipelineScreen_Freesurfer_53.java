package org.nrg.xnat.turbine.modules.screens;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.nrg.config.entities.Configuration;
import org.nrg.xdat.model.ArcPipelinedataI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatQcmanualassessordataI;
import org.nrg.xdat.model.XnatQcscandataI;
import org.nrg.xdat.om.*;
import org.nrg.xdat.om.base.BaseXnatProjectdata;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.nrg.xdat.XDAT;


import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

public class PipelineScreen_Freesurfer_53 extends DefaultPipelineScreen {


    public final String SCANTYPE_CONFIG_TOOL_NAME = "params";
    public final String SCANTYPE_CONFIG_FILE_NAME = "mr_scan_types";
    public final String T2_CONFIG_TOOL_NAME = "params";
    public final String T2_CONFIG_FILE_NAME = "t2_scan_types";
    public final String FLAIR_CONFIG_TOOL_NAME = "params";
    public final String FLAIR_CONFIG_FILE_NAME = "flair_scan_types";


    static Logger logger = Logger.getLogger(PipelineScreen_Freesurfer_53.class);
    public void finalProcessing(RunData data, Context context){


        try {
            String projectId = (String)TurbineUtils.GetPassedParameter("project",data);
            String pipelinePath = (String)TurbineUtils.GetPassedParameter("pipeline",data);
            String schemaType = (String)TurbineUtils.GetPassedParameter("schema_type",data);
            ArcProject arcProject = ArcSpecManager.GetFreshInstance().getProjectArc(projectId);

            if (schemaType.equals(XnatProjectdata.SCHEMA_ELEMENT_NAME)) {
                ArcProjectPipeline pipelineData = (ArcProjectPipeline)arcProject.getPipelineByPath(pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }else {
                ArcPipelinedataI pipelineData = arcProject.getPipelineForDescendantByPath(schemaType, pipelinePath);
                context.put("pipeline", pipelineData);
                setParameters(pipelineData, context);
            }

            if (!(om instanceof XnatImagesessiondata)) {
                String m ="Cannot run Freesurfer on a non-image session.";
                data.setMessage(m);
                throw new Exception(m);
            }

            Long projectIdNum = BaseXnatProjectdata.getProjectInfoIdFromStringId(projectId);
            ArrayList<XnatImagescandata> mrScans;
            ArrayList<XnatImagescandata> t2Scans;
            ArrayList<XnatImagescandata> flairScans;
            try {
                mrScans = getScansFromScantypeConfig(SCANTYPE_CONFIG_TOOL_NAME, SCANTYPE_CONFIG_FILE_NAME, projectIdNum, true);
                t2Scans = getScansFromScantypeConfig(T2_CONFIG_TOOL_NAME, T2_CONFIG_FILE_NAME, projectIdNum, false);
                flairScans = getScansFromScantypeConfig(FLAIR_CONFIG_TOOL_NAME, FLAIR_CONFIG_FILE_NAME, projectIdNum, false);
            } catch (Exception e) {
                data.setMessage(e.getMessage());
                throw e;
            }

            ArrayList<XnatImagescandata> structuralScans = new ArrayList<XnatImagescandata>();
            if (mrScans != null) {
                context.put("mrScans", mrScans);
                structuralScans.addAll(mrScans);
            }
            if (t2Scans != null) {
                context.put("t2Scans", t2Scans);
                structuralScans.addAll(t2Scans);
            }
            if (flairScans != null) {
                context.put("flairScans", flairScans);
                structuralScans.addAll(flairScans);
            }


            HashMap<String,ArrayList<String>> allScanResourceLabels = new HashMap<String,ArrayList<String>>();
            for (XnatImagescandata scan : structuralScans) {
                ArrayList<String> scanResourceLabels = new ArrayList<String>();
                for ( XnatAbstractresourceI scanResource : scan.getFile() ) {
                    if (!(scanResource instanceof XnatResourcecatalog)) {
                        continue;
                    }
                    String scanResourceLabel = scanResource.getLabel();
                    if (StringUtils.isNotBlank(scanResourceLabel)) {
                        scanResourceLabels.add(scanResourceLabel);
                    } else {
                        scanResourceLabels.add("null");
                    }
                }
                allScanResourceLabels.put(scan.getId(),scanResourceLabels);
            }
            context.put("allScanResourceLabels",allScanResourceLabels);

            String bestScan = "";
            String qcFlagParam = projectParameters.get("use_manual_qc").getCsvvalues();
            String qcScaleParam = projectParameters.get("qc_rating_scale").getCsvvalues();
            String qcRatingParam = projectParameters.get("qc_rating").getCsvvalues();
            if (StringUtils.isNotBlank(qcFlagParam) && qcFlagParam.equals("1")) {
                if (StringUtils.isBlank(qcScaleParam) || StringUtils.isBlank(qcRatingParam)) {
                    String m = "Could not find Manual QC scale or rating.<br/>Make sure the QC parameters are not blank in project settings.";
                    data.setMessage(m);
                    throw new Exception(m);
                }
                XnatQcmanualassessordataI qc = ((XnatImagesessiondata) om).getManualQC();
                if (qc != null) {
                    List<XnatQcscandataI> qcScans = qc.getScans_scan();
                    if (qcScans != null) {
                        for (XnatQcscandataI qcScan : qcScans) {
                            String qcRatingScale = qcScan.getRating_scale();
                            String qcRating = qcScan.getRating();
                            if (qcScaleParam.equals(qcRatingScale) && qcRatingParam.equals(qcRating)) {
                                bestScan = qcScan.getImagescanId();
                                break;
                            }
                        }
                    }
                }
                if (bestScan.equals("")) {
                    String m = "Cannot run Freesurfer. Could not find best scan from manual QC.";
                    data.setMessage(m);
                    throw new Exception(m);
                }
            }
            context.put("bestScan",bestScan);

            // if (! (om instanceof XnatMrsessiondata || om instanceof XnatPetsessiondata)) {
            //     throw new Exception("om should be of type XnatMrsessiondata or XnatPetsessiondata");
            // }
            // if (om instanceof XnatMrsessiondata) {
            //     XnatMrsessiondata mr = (XnatMrsessiondata) om;

            //     ArcPipelineparameterdata mprageParam = getProjectPipelineSetting(MPRAGE_PARAM);
            //     if (mprageParam != null) {
            //         mprageScanType = mprageParam.getCsvvalues();
            //         setHasDicomFiles(mr, mprageScanType, context);
            //     }

            // } else if (om instanceof XnatPetsessiondata) {
            //     XnatPetsessiondata petmr = (XnatPetsessiondata) om;

            //     ArcPipelineparameterdata mprageParam = getProjectPipelineSetting(MPRAGE_PARAM);
            //     if (mprageParam != null) {
            //         mprageScanType = mprageParam.getCsvvalues();
            //         setHasDicomFiles(petmr, mprageScanType, context);
            //     }
            // }

            // LinkedHashMap<String,String> buildableScanTypes = new LinkedHashMap<String,String>();
            // if (mprageScanType != null) {
            //     buildableScanTypes.put(MPRAGE, mprageScanType);
            // }
            // context.put("buildableScanTypes", buildableScanTypes);
            context.put("projectParameters", projectParameters);
            // context.put("mpragekey", MPRAGE);

        }catch(Exception e) {
            logger.error("Possibly the project wide pipeline has not been set", e);
            e.printStackTrace();
        }
    }

    public ArrayList<XnatImagescandata> getScansFromScantypeConfig(String toolName, String fileName, Long projectIdNum, Boolean isRequired) throws Exception {
        ArrayList<XnatImagescandata> allScans = new ArrayList<XnatImagescandata>();

        Configuration config = XDAT.getConfigService().getConfig(toolName, fileName, projectIdNum);
        if (config == null || config.getContents() == null) {
            if (isRequired) {
                String m = "An error has occurred. Could not find scan types from required config " + toolName + "/" + fileName + ", thus could not find scans.";
                throw new Exception(m);
            }
        } else {
            String scanTypeCsv = config.getContents().trim();

            for (String scanType : scanTypeCsv.split(",")) {
                ArrayList<XnatImagescandata> scans = ((XnatImagesessiondata) om).getScansByType(scanType);
                if (scans != null && !scans.isEmpty()) {
                    allScans.addAll(scans);
                }
            }

            if (allScans.isEmpty() && isRequired) {
                String m = "Cannot run Freesurfer. There are no scans of type "+scanTypeCsv+" in the session.";
                throw new Exception(m);
            }
        }

        return allScans;
    }
}
