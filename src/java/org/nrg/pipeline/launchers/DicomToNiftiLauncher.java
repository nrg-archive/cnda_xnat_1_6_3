/*
 *  Copyright Washington University in St Louis 2006
 *  All rights reserved
 *
 *  @author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.utils.PipelineUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

public class DicomToNiftiLauncher extends PipelineLauncher{

    public static final String NAME = "DicomToNifti.xml";
    public static final String LOCATION = "DicomToNifti";
    public static final String CATALOG = "/data/CNDA/pipeline/catalog/";
    public static final String PIPELINENAME = CATALOG+File.separator+LOCATION+File.separator+NAME;
    public static final String TEMPLATE = "PipelineScreen_DicomToNifti.vm";

    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DicomToNiftiLauncher.class);

    public boolean launch(RunData data, Context context) {
        try {
            ItemI data_item = TurbineUtils.GetItemBySearch(data);
            XnatImagesessiondata imageSession = new XnatImagesessiondata(data_item);
            XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, imageSession);
            String pipelineName = PIPELINENAME;
            String cmdPrefix = data.getParameters().get("cmdprefix");
            xnatPipelineLauncher.setPipelineName(pipelineName);
            String buildDir = PipelineFileUtils.getBuildDir(imageSession.getProject(), true);
            buildDir += "DicomToNifti";
            xnatPipelineLauncher.setBuildDir(buildDir);
            xnatPipelineLauncher.setNeedsBuildDir(false);

            Parameters parameters = Parameters.Factory.newInstance();
            ParameterData param = parameters.addNewParameter();
            param.setName("create_nii");
            param.addNewValues().setUnique(data.getParameters().getString("create_nii"));

            param = parameters.addNewParameter();
            param.setName("overwrite");
            param.addNewValues().setUnique(data.getParameters().getString("overwrite"));

            // String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
            // String[] emails = emailsStr.trim().split(",");
            // for (int i = 0 ; i < emails.length; i++) {
            //     if (emails[i]!=null && !emails[i].equals("")) xnatPipelineLauncher.notify(emails[i]);
            // }

            String paramFileName = getName(pipelineName);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String s = formatter.format(date);

            paramFileName += "_params_" + s + ".xml";

            String paramFilePath = saveParameters(buildDir + File.separator + imageSession.getLabel(),paramFileName,parameters);

            xnatPipelineLauncher.setParameterFile(paramFilePath);

            return xnatPipelineLauncher.launch(cmdPrefix);
        }catch(Exception e) {
            logger.error(e.getCause() + " " + e.getLocalizedMessage());
            return false;
        }
    }

}
