/*
 * org.nrg.pipeline.launchers.FaceMaskingLauncher
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 08/27/13
*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class FaceMaskingLauncher extends PipelineLauncher{
    ArrayList<String> mprageScans = null;
    static org.apache.log4j.Logger logger = Logger.getLogger(FaceMaskingLauncher.class);

    public FaceMaskingLauncher(ArrayList<String> mprs) {
        mprageScans = mprs;
    }

    public FaceMaskingLauncher(RunData data) {
        mprageScans = getCheckBoxSelections(data,"MPRAGE");
    }

    // Need to override this abstract method in PipelineLauncher
    public boolean launch(RunData data, Context context) {
        return false;
    }


    public boolean launch(RunData data, Context context, XnatImagesessiondata image) {
        try {
            XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, image);

            String pipelineName = ((String)TurbineUtils.GetPassedParameter("facemasking_pipelinename",data));
            String cmdPrefix = ((String)TurbineUtils.GetPassedParameter("cmdprefix",data));

            xnatPipelineLauncher.setPipelineName(pipelineName);
            xnatPipelineLauncher.setSupressNotification(true);

            String buildDir = PipelineFileUtils.getBuildDir(image.getProject(), true);
            buildDir +=  "facemask"  ;

            xnatPipelineLauncher.setBuildDir(buildDir);
            xnatPipelineLauncher.setNeedsBuildDir(false);

            Parameters parameters = Parameters.Factory.newInstance();

            ParameterData param = parameters.addNewParameter();
            param.setName("sessionLabel");
            param.addNewValues().setUnique(image.getLabel());

            param = parameters.addNewParameter();
            param.setName("sessionId");
            param.addNewValues().setUnique(image.getId());

            param = parameters.addNewParameter();
            param.setName("subject");
            param.addNewValues().setUnique(image.getSubjectId());

            // text entry boxes: invasiveness, threshold, rois, dirs (last two included with "use_manual_roi")
            param = parameters.addNewParameter();
            param.setName("invasiveness");
            param.addNewValues().setUnique(((String)TurbineUtils.GetPassedParameter("invasiveness",data)));

            param = parameters.addNewParameter();
            param.setName("threshold");
            param.addNewValues().setUnique(((String)TurbineUtils.GetPassedParameter("threshold",data)));

            // Ref scan. Get session and scan number from value string.
            String[] refStrs = ((String)TurbineUtils.GetPassedParameter("ref",data)).split("-");
            param = parameters.addNewParameter();
            param.setName("ref");
            param.addNewValues().setUnique(refStrs[1]);

            param = parameters.addNewParameter();
            param.setName("ref_session");
            param.addNewValues().setUnique(refStrs[0]);

            // MPRAGE list
            param = parameters.addNewParameter();
            param.setName("scanids");
            Values values = param.addNewValues();
            if (mprageScans.size() == 1) {
                values.setUnique(mprageScans.get(0));
            }else {
                for (int i = 0; i < mprageScans.size(); i++) {
                    values.addList(mprageScans.get(i));
                }
            }

            // checkboxes: usebet, maskears, use_manual_roi
            param = parameters.addNewParameter();
            param.setName("usebet");
            if (TurbineUtils.HasPassedParameter("usebet", data)) {
                param.addNewValues().setUnique("1");
            }else {
                param.addNewValues().setUnique("0");
            }

            param = parameters.addNewParameter();
            param.setName("maskears");
            if (TurbineUtils.HasPassedParameter("maskears", data)) {
                param.addNewValues().setUnique("1");
            }else {
                param.addNewValues().setUnique("0");
            }

            param = parameters.addNewParameter();
            param.setName("use_manual_roi");
            if (TurbineUtils.HasPassedParameter("use_manual_roi", data)) {
                param.addNewValues().setUnique("1");

                param = parameters.addNewParameter();
                param.setName("rois");
                param.addNewValues().setUnique(((String)TurbineUtils.GetPassedParameter("rois",data)));

                param = parameters.addNewParameter();
                param.setName("dirs");
                param.addNewValues().setUnique(((String)TurbineUtils.GetPassedParameter("dirs",data)));
            }else {
                param.addNewValues().setUnique("0");

                param = parameters.addNewParameter();
                param.setName("rois");
                param.addNewValues().setUnique("0");

                param = parameters.addNewParameter();
                param.setName("dirs");
                param.addNewValues().setUnique("0");
            }

            String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
            String[] emails = emailsStr.trim().split(",");
            for (int i = 0 ; i < emails.length; i++) {
                if (emails[i]!=null && !emails[i].equals(""))  xnatPipelineLauncher.notify(emails[i]);
            }

            String paramFileName = getName(pipelineName);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String s = formatter.format(date);

            paramFileName += "_params_" + s + ".xml";

            String paramFilePath = saveParameters(buildDir + File.separator + image.getLabel(),paramFileName,parameters);

            xnatPipelineLauncher.setParameterFile(paramFilePath);

            boolean rtn = xnatPipelineLauncher.launch(cmdPrefix);
            return rtn;
        }catch(Exception e) {
            logger.error(e.getCause() + " " + e.getLocalizedMessage());
            return false;
        }
    }

}
