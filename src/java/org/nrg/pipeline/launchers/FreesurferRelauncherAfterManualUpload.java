package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.FsFsdata;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class FreesurferRelauncherAfterManualUpload extends PipelineLauncher{
    static org.apache.log4j.Logger logger = Logger.getLogger(FreesurferRelauncher.class);
    final static String PIPELINE_NAME = "Freesurfer/Freesurfer_relaunch.xml";
    FsFsdata fs = null;

    public FreesurferRelauncherAfterManualUpload(FsFsdata fs) {
        this.fs = fs;
    }


    public boolean launch(RunData data, Context context) {
        boolean launch_success = false;
        try {
        XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetBareLauncherForExperiment(data, context, fs);
        xnatPipelineLauncher.setPipelineName(PIPELINE_NAME);
        xnatPipelineLauncher.setSupressNotification(true);
        xnatPipelineLauncher.setId(fs.getId());
        xnatPipelineLauncher.setLabel(fs.getLabel());
        xnatPipelineLauncher.setDataType(fs.getXSIType());
        xnatPipelineLauncher.setExternalId(fs.getProject());

        String buildDir = PipelineFileUtils.getBuildDir(fs.getProject(), true);
        buildDir +=  "fsrfer"  ;
        xnatPipelineLauncher.setBuildDir(buildDir);
        xnatPipelineLauncher.setNeedsBuildDir(false);

        Parameters parameters = Parameters.Factory.newInstance();

        ParameterData param = parameters.addNewParameter();
        param.setName("fsId");
        param.addNewValues().setUnique(fs.getId());

        param = parameters.addNewParameter();
        param.setName("fsLabel");
        param.addNewValues().setUnique(fs.getLabel());


        param = parameters.addNewParameter();
        param.setName("subjectlabel");
        param.addNewValues().setUnique(fs.getImageSessionData().getSubjectData().getLabel());

        param = parameters.addNewParameter();
        param.setName("mrLabel");
        param.addNewValues().setUnique(fs.getImageSessionData().getLabel());

        param = parameters.addNewParameter();
        param.setName("mrId");
        param.addNewValues().setUnique(fs.getImageSessionData().getId());

        // param = parameters.addNewParameter();
        // param.setName("archivePath");
        // param.addNewValues().setUnique(fs.getImageSessionData().getArchivePath()+ fs.getImageSessionData().getLabel() + File.separator +"ASSESSORS"+ File.separator + fs.getArchiveDirectoryName());


        param = parameters.addNewParameter();
        param.setName("project");
        param.addNewValues().setUnique(fs.getProject());

        String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
        String[] emails = emailsStr.trim().split(",");
        for (int i = 0 ; i < emails.length; i++) {
            if (emails[i]!=null && !emails[i].equals(""))  xnatPipelineLauncher.notify(emails[i]);
        }

        String paramFileName = getName(PIPELINE_NAME);
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String s = formatter.format(date);

        paramFileName += "_params_" + s + ".xml";

        String paramFilePath = saveParameters(buildDir+File.separator + fs.getLabel(),paramFileName,parameters);

        xnatPipelineLauncher.setParameterFile(paramFilePath);
        launch_success = xnatPipelineLauncher.launch();

        }catch(Exception e) {
            logger.error(e);
        }

        return launch_success ;

    }
}
