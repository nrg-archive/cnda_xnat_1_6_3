//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:37 CDT 2005
 *
 */
package org.nrg.xdat.om;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.base.BaseXnatImageassessordata;
import org.nrg.xdat.om.base.BaseXnatMrsessiondata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.InvalidValueException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.search.TableSearch;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.StringUtils;

/**
 * @author XDAT
 *
 */
public class XnatMrsessiondata extends BaseXnatMrsessiondata {
    
    private CndaRadiologyreaddata radRead = null;
    private CndaAtlasscalingfactordata asf = null;
    private CndaSegmentationfastdata fseg = null;
    private CndaModifiedscheltensdata wm = null;
    
	public XnatMrsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatMrsessiondata(UserI user)
	{
		super(user);
	}

	public XnatMrsessiondata()
	{}

	public XnatMrsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}
	
	public ArrayList getMinimalLoadAssessors()
	{
	    if (minLoadAssessors==null)
	    {
	        minLoadAssessors = new ArrayList();
	        if(getItem().isPreLoaded())
	        {
	            minLoadAssessors=this.getAssessors();
	            
	            for (int i=0;i< minLoadAssessors.size();i++)
	            {
	                ItemI child = (ItemI)minLoadAssessors.get(i);
	                String element = child.getXSIType();
	                if (element.equalsIgnoreCase("cnda:radiologyReadData"))
                    {
                        if (this.radRead == null)
                        {
                            radRead = new CndaRadiologyreaddata(child); 
                        }
                    }else if (element.equalsIgnoreCase("cnda:atlasScalingFactorData"))
                    {
                        if (this.asf == null)
                        {
                           asf = new CndaAtlasscalingfactordata(child);
                                
                        }
                    }else if (element.equalsIgnoreCase("cnda:segmentationFastData"))
                    {
                        if (this.fseg == null)
                        {
                            fseg = new CndaSegmentationfastdata(child);
                                
                        }
                    }else if (element.equalsIgnoreCase("cnda:modifiedScheltensData"))
                    {
                        if (this.wm == null)
                        {
                            wm = new CndaModifiedscheltensdata(child);
                        }
                    }
	            }
	        }else{
	        try {
                XFTTable table = TableSearch.Execute("SELECT ex.id,ex.date,ex.project,me.element_name AS type,me.element_name,ex.note AS note,i.lastname, investigator_xnat_investigatorData_id AS invest_id,projects FROM xnat_imageAssessorData assessor LEFT JOIN xnat_experimentData ex ON assessor.ID=ex.ID LEFT JOIN xnat_investigatorData i ON i.xnat_investigatorData_id=ex.investigator_xnat_investigatorData_id LEFT JOIN xdat_meta_element me ON ex.extension=me.xdat_meta_element_id LEFT JOIN (SELECT xs_a_concat(project || ',') AS PROJECTS, sharing_share_xnat_experimentda_id FROM xnat_experimentData_share GROUP BY sharing_share_xnat_experimentda_id) PROJECT_SEARCH ON ex.id=PROJECT_SEARCH.sharing_share_xnat_experimentda_id WHERE assessor.imagesession_id='" + this.getId() +"' ORDER BY ex.date ASC",getDBName(),null);
                table.resetRowCursor();
                while (table.hasMoreRows())
                {
                    Hashtable row = table.nextRowHash();
                    String element = (String)row.get("element_name");
                    try {
                        XFTItem child = XFTItem.NewItem(element,this.getUser());
                        
                        Object date = row.get("date");
                        Object id = row.get("id");
                        Object type = row.get("type");
                        Object note = row.get("note");
                        Object invest_id = row.get("invest_id");
                        Object lastname = row.get("lastname");
                        Object project = row.get("project");
                        
                        if (date!=null)
                        {
                            try {
                                child.setProperty("date",date);
                            } catch (XFTInitException e) {
                                logger.error("",e);
                            } catch (ElementNotFoundException e) {
                                logger.error("",e);
                            } catch (FieldNotFoundException e) {
                                logger.error("",e);
                            } catch (InvalidValueException e) {
                                logger.error("",e);
                            }
                        }
                        if (id!=null)
                        {
                            try {
                                child.setProperty("ID",id);
                            } catch (XFTInitException e) {
                                logger.error("",e);
                            } catch (ElementNotFoundException e) {
                                logger.error("",e);
                            } catch (FieldNotFoundException e) {
                                logger.error("",e);
                            } catch (InvalidValueException e) {
                                logger.error("",e);
                            }
                        }
                        
                        if (project!=null)
                        {
                            try {
                                child.setProperty("project",project);
                            } catch (XFTInitException e) {
                                logger.error("",e);
                            } catch (ElementNotFoundException e) {
                                logger.error("",e);
                            } catch (FieldNotFoundException e) {
                                logger.error("",e);
                            } catch (InvalidValueException e) {
                                logger.error("",e);
                            }
                        }
                        
                        if (note!=null)
                        {
                            try {
                                child.setProperty("note",note);
                            } catch (XFTInitException e) {
                                logger.error("",e);
                            } catch (ElementNotFoundException e) {
                                logger.error("",e);
                            } catch (FieldNotFoundException e) {
                                logger.error("",e);
                            } catch (InvalidValueException e) {
                                logger.error("",e);
                            }
                        }
                        if (lastname!=null)
                        {
                            try {
                                child.setProperty("investigator.lastname",lastname);
                            } catch (XFTInitException e) {
                                logger.error("",e);
                            } catch (ElementNotFoundException e) {
                                logger.error("",e);
                            } catch (FieldNotFoundException e) {
                                logger.error("",e);
                            } catch (InvalidValueException e) {
                                logger.error("",e);
                            }
                        }
                        if (invest_id!=null)
                        {
                            try {
                                child.setProperty("investigator_xnat_investigatorData_id",invest_id);
                            } catch (XFTInitException e) {
                                logger.error("",e);
                            } catch (ElementNotFoundException e) {
                                logger.error("",e);
                            } catch (FieldNotFoundException e) {
                                logger.error("",e);
                            } catch (InvalidValueException e) {
                                logger.error("",e);
                            }
                        }


                        String projects = (String)row.get("projects");
                        if (projects!=null)
                        {
                            Iterator iter= StringUtils.CommaDelimitedStringToArrayList(projects, true).iterator();
                            while(iter.hasNext())
                            {
                               String projectName = (String)iter.next();
                               child.setProperty("sharing.share.project", projectName);
                            }                   
                        }    
                        
                        if (element.equalsIgnoreCase("cnda:radiologyReadData"))
                        {
                            if (this.radRead == null)
                            {
                                if (this.getUser().canRead(child))
                                {
                                    radRead = new CndaRadiologyreaddata(child.getCurrentDBVersion(false));
                                    minLoadAssessors.add(radRead);
                                }else{
                                    minLoadAssessors.add(new CndaRadiologyreaddata(child));
                                }
                            }else{
                                minLoadAssessors.add(new CndaRadiologyreaddata(child));
                            }
                        }else if (element.equalsIgnoreCase("cnda:atlasScalingFactorData"))
                        {
                            if (this.asf == null)
                            {
                                if (this.getUser().canRead(child))
                                {
                                    asf = new CndaAtlasscalingfactordata(child.getCurrentDBVersion(false));
                                    minLoadAssessors.add(asf);
                                }else{
                                    minLoadAssessors.add(new CndaAtlasscalingfactordata(child));
                                }
                            }else{
                                minLoadAssessors.add(new CndaAtlasscalingfactordata(child));
                            }
                        }else if (element.equalsIgnoreCase("cnda:segmentationFastData"))
                        {
                            if (this.fseg == null)
                            {
                                if (this.getUser().canRead(child))
                                {
                                    fseg = new CndaSegmentationfastdata(child.getCurrentDBVersion(false));
                                    minLoadAssessors.add(fseg);
                                }else{
                                    minLoadAssessors.add(new CndaSegmentationfastdata(child));
                                }
                            }else{
                                minLoadAssessors.add(new CndaSegmentationfastdata(child));
                            }
                        }else if (element.equalsIgnoreCase("cnda:modifiedScheltensData"))
                        {
                            if (this.wm == null)
                            {
                                if (this.getUser().canRead(child))
                                {
                                    wm = new CndaModifiedscheltensdata(child.getCurrentDBVersion(false));
                                    minLoadAssessors.add(wm);
                                }else{
                                    minLoadAssessors.add(new CndaModifiedscheltensdata(child));
                                }
                            }else{
                                minLoadAssessors.add(new CndaModifiedscheltensdata(child));
                            }
                        }else if (element.equalsIgnoreCase(XnatQcmanualassessordata.SCHEMA_ELEMENT_NAME))
                        {
                            if (this.manQC == null)
                            {
                                if (this.getUser().canRead(child))
                                {
                                    this.manQC = new XnatQcmanualassessordata(child.getCurrentDBVersion(false));
                                    minLoadAssessors.add(this.manQC);
                                }else{
                                    minLoadAssessors.add(new XnatQcmanualassessordata(child));
                                }
                            }else{
                                minLoadAssessors.add(new XnatQcmanualassessordata(child));
                            }
                        }else if (element.equalsIgnoreCase(XnatQcassessmentdata.SCHEMA_ELEMENT_NAME))
                        {
                            if (this.qc == null)
                            {
                                if (this.getUser().canRead(child))
                                {
                                    this.qc = new XnatQcassessmentdata(child.getCurrentDBVersion(false));
                                    minLoadAssessors.add(this.qc);
                                }else{
                                    minLoadAssessors.add(new XnatQcassessmentdata(child));
                                }
                            }else{
                                minLoadAssessors.add(new XnatQcassessmentdata(child.getCurrentDBVersion(false)));
                            }
                        }else{
                            minLoadAssessors.add((XnatImageassessordata)BaseElement.GetGeneratedItem(child));
                        }
                    } catch (XFTInitException e) {
                        logger.error("",e);
                    } catch (ElementNotFoundException e) {
                        logger.error("",e);
                    }
                }
            } catch (Exception e) {
                logger.error("",e);
            }
	        }
	    }
	    
	    return (ArrayList)minLoadAssessors;
	}
	
    /**
     * @return Returns the radRead.
     */
    public CndaRadiologyreaddata getRadRead() {
        return radRead;
    }
    /**
     * @param radRead The radRead to set.
     */
    public void setRadRead(CndaRadiologyreaddata radRead) {
        this.radRead = radRead;
    }
    
    public Hashtable getScanHash()
    {
        Hashtable ScansHash =new Hashtable();
    	try {
    	  List stc = new ArrayList();
    	  if (getSessionType()== null) {
				stc.add("MPRAGE");
				stc.add("FLASH");
				stc.add("FLAIR");
				stc.add("DTI");
			    stc.add("TSE");
			    //stc.add("BOLD");
    	  }else {
    	   	  if (!getSessionType().equals("Gleek")) {
        	  		stc.add("MPRAGE");
				stc.add("FLASH");
				stc.add("FLAIR");
				stc.add("DTI");
				stc.add("TSE");
				//stc.add("BOLD");
    	  		}else if (getSessionType().equals("Gleek")) {
					stc.add("MEF5");
					stc.add("MEF30");
					stc.add("MTC5"); 
					stc.add("MPRAGE");
					stc.add("FLAIR");	
    	  		}
    	  }		
    	  for (int i=0; i<stc.size();i++) {
	  	
    	  	List scans = getScansByType((String)stc.get(i));
    	  	
			if (scans!=null && scans.size()>0) {
				ScansHash.put((String)stc.get(i), scans);
			}
    	  }
    	  
    	}catch(Exception te) {
    		te.printStackTrace();  	
    	}
    	return ScansHash;
    }
    /**
     * @return Returns the asf.
     */
    public CndaAtlasscalingfactordata getAsf() {
        return asf;
    }
    /**
     * @param asf The asf to set.
     */
    public void setAsf(CndaAtlasscalingfactordata asf) {
        this.asf = asf;
    }
    /**
     * @return Returns the fseg.
     */
    public CndaSegmentationfastdata getFseg() {
        return fseg;
    }
    /**
     * @param fseg The fseg to set.
     */
    public void setFseg(CndaSegmentationfastdata fseg) {
        this.fseg = fseg;
    }
    /**
     * @return Returns the wm.
     */
    public CndaModifiedscheltensdata getWm() {
        return wm;
    }
    /**
     * @param wm The wm to set.
     */
    public void setWm(CndaModifiedscheltensdata wm) {
        this.wm = wm;
    }
    
	   public static ArrayList<XnatImagesessiondata> GetAllPreviousImagingSessions(XnatImagesessiondata latestImagingSession, String projectId, XDATUser user) {
	    	String subject_id = latestImagingSession.getSubjectId();
	    	ArrayList<XnatImagesessiondata> previousSessions = new ArrayList<XnatImagesessiondata>();
	        String login = null;
			if (user != null)	{
			    login = user.getUsername();
			}
	        String query = "SELECT mr.id FROM xnat_mrSessionData mr LEFT JOIN xnat_subjectAssessorData sad ON mr.ID=sad.ID LEFT JOIN xnat_experimentData ed ON sad.ID=ed.ID left join xnat_imagesessiondata im on ed.id = im.id  WHERE subject_id='" + subject_id +"' and ed.project='" + projectId + "' and date < '" +  latestImagingSession.getDate() +"' ORDER BY date DESC ";
	        try {
	            XFTTable table = TableSearch.Execute(query,user.getDBName(),login);
	            if (table.size()>0)            {
	                table.resetRowCursor();
	                Object mr_id = null;
	                while(table.hasMoreRows())    {
	                     mr_id = table.nextRowHash().get("id");
	                    if (mr_id !=null)      {
	                    	  ItemI mr = ItemSearch.GetItem("xnat:mrSessionData.ID",(String)mr_id,user,false);
	                    	  if (mr != null) {
	                    		  previousSessions.add(new XnatMrsessiondata(mr));
	                    	  }
	                    }
	                }
	            }
	            return previousSessions;
	        } catch (Exception e) {
	            logger.error("",e);
	            return null;
	        }
	    }

  
}
