/*
 * GENERATED FILE
 * Created on Mon Oct 03 20:20:14 IST 2011
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
public interface PetFspettimecoursedataRoiI {

	public String getSchemaElementName();

	/**
	 * @return Returns the NVox.
	 */
	public Integer getNvox();

	/**
	 * Sets the value for NVox.
	 * @param v Value to Set.
	 */
	public void setNvox(Integer v);

	/**
	 * @return Returns the BP.
	 */
	public Double getBp();

	/**
	 * Sets the value for BP.
	 * @param v Value to Set.
	 */
	public void setBp(Double v);

	/**
	 * @return Returns the R2_BP.
	 */
	public Double getR2Bp();

	/**
	 * Sets the value for R2_BP.
	 * @param v Value to Set.
	 */
	public void setR2Bp(Double v);

	/**
	 * @return Returns the INTC_BP.
	 */
	public Double getIntcBp();

	/**
	 * Sets the value for INTC_BP.
	 * @param v Value to Set.
	 */
	public void setIntcBp(Double v);

	/**
	 * @return Returns the BP_RSF.
	 */
	public Double getBpRsf();

	/**
	 * Sets the value for BP_RSF.
	 * @param v Value to Set.
	 */
	public void setBpRsf(Double v);

	/**
	 * @return Returns the R2_RSF.
	 */
	public Double getR2Rsf();

	/**
	 * Sets the value for R2_RSF.
	 * @param v Value to Set.
	 */
	public void setR2Rsf(Double v);

	/**
	 * @return Returns the INTC_RSF.
	 */
	public Double getIntcRsf();

	/**
	 * Sets the value for INTC_RSF.
	 * @param v Value to Set.
	 */
	public void setIntcRsf(Double v);

	/**
	 * @return Returns the BP_PVC2C.
	 */
	public Double getBpPvc2c();

	/**
	 * Sets the value for BP_PVC2C.
	 * @param v Value to Set.
	 */
	public void setBpPvc2c(Double v);

	/**
	 * @return Returns the R2_PVC2C.
	 */
	public Double getR2Pvc2c();

	/**
	 * Sets the value for R2_PVC2C.
	 * @param v Value to Set.
	 */
	public void setR2Pvc2c(Double v);

	/**
	 * @return Returns the INTC_PVC2C.
	 */
	public Double getIntcPvc2c();

	/**
	 * Sets the value for INTC_PVC2C.
	 * @param v Value to Set.
	 */
	public void setIntcPvc2c(Double v);

	/**
	 * @return Returns the SUVR.
	 */
	public Double getSuvr();

	/**
	 * Sets the value for SUVR.
	 * @param v Value to Set.
	 */
	public void setSuvr(Double v);

	/**
	 * @return Returns the SUVR_RSF.
	 */
	public Double getSuvrRsf();

	/**
	 * Sets the value for SUVR_RSF.
	 * @param v Value to Set.
	 */
	public void setSuvrRsf(Double v);

	/**
	 * @return Returns the SUVR_PVC2C.
	 */
	public Double getSuvrPvc2c();

	/**
	 * Sets the value for SUVR_PVC2C.
	 * @param v Value to Set.
	 */
	public void setSuvrPvc2c(Double v);

	/**
	 * addStats
	 * @return Returns an ArrayList of org.nrg.xdat.om.XnatAddfieldI
	 */
	public ArrayList<org.nrg.xdat.om.XnatAddfield> getAddstats();

	/**
	 * Sets the value for addStats.
	 * @param v Value to Set.
	 */
	public void setAddstats(ItemI v) throws Exception;

	/**
	 * @return Returns the name.
	 */
	public String getName();

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v);

	/**
	 * @return Returns the pet_fspetTimeCourseData_roi_id.
	 */
	public Integer getPetFspettimecoursedataRoiId();

	/**
	 * Sets the value for pet_fspetTimeCourseData_roi_id.
	 * @param v Value to Set.
	 */
	public void setPetFspettimecoursedataRoiId(Integer v);
}
