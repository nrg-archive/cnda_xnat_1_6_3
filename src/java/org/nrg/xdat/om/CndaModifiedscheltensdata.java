//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:38 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaModifiedscheltensdata extends BaseCndaModifiedscheltensdata {

	public CndaModifiedscheltensdata(ItemI item)
	{
		super(item);
	}

	public CndaModifiedscheltensdata(UserI user)
	{
		super(user);
	}

	public CndaModifiedscheltensdata()
	{}

	public CndaModifiedscheltensdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

    public Integer getScheltensDwmFrontal(){
		Integer dwmFrontalScheltens = this.getAssessment_regions_deepWhiteMatter_frontal().getScheltens();
		if (dwmFrontalScheltens == null){
			if (this.getAssessment_regions_deepWhiteMatter_frontal().getLeft_confluent().booleanValue() || getAssessment_regions_deepWhiteMatter_frontal().getRight_confluent().booleanValue() ){
				dwmFrontalScheltens = new Integer(6);
			} else if (this.getAssessment_regions_deepWhiteMatter_frontal().getLeft_lesions_large().intValue() + getAssessment_regions_deepWhiteMatter_frontal().getRight_lesions_large().intValue() > 0){
				dwmFrontalScheltens = new Integer(5);
			} else if (getAssessment_regions_deepWhiteMatter_frontal().getLeft_lesions_medium().intValue() + getAssessment_regions_deepWhiteMatter_frontal().getRight_lesions_medium().intValue() > 5 ){
				dwmFrontalScheltens = new Integer(4);
			} else if (getAssessment_regions_deepWhiteMatter_frontal().getLeft_lesions_medium().intValue() + getAssessment_regions_deepWhiteMatter_frontal().getRight_lesions_medium().intValue() > 0){
				dwmFrontalScheltens = new Integer(3);
			} else if (getAssessment_regions_deepWhiteMatter_frontal().getLeft_lesions_small().intValue() + getAssessment_regions_deepWhiteMatter_frontal().getRight_lesions_small().intValue() > 5){
				dwmFrontalScheltens = new Integer(2);
			} else if (getAssessment_regions_deepWhiteMatter_frontal().getLeft_lesions_small().intValue() + getAssessment_regions_deepWhiteMatter_frontal().getRight_lesions_small().intValue() > 0){
				dwmFrontalScheltens = new Integer(1);
			} else {	
				dwmFrontalScheltens = new Integer(0);
			}
		}

		return dwmFrontalScheltens;
	}

	public Integer getScheltensDwmParietal(){
		Integer dwmParietalScheltens = this.getAssessment_regions_deepWhiteMatter_parietal().getScheltens();
		if (dwmParietalScheltens == null){
		    if (this.getAssessment_regions_deepWhiteMatter_parietal().getLeft_confluent().booleanValue() || getAssessment_regions_deepWhiteMatter_parietal().getRight_confluent().booleanValue() ){
		        dwmParietalScheltens = new Integer(6);
			} else if (this.getAssessment_regions_deepWhiteMatter_parietal().getLeft_lesions_large().intValue() + getAssessment_regions_deepWhiteMatter_parietal().getRight_lesions_large().intValue() > 0){
			    dwmParietalScheltens = new Integer(5);
			} else if (getAssessment_regions_deepWhiteMatter_parietal().getLeft_lesions_medium().intValue() + getAssessment_regions_deepWhiteMatter_parietal().getRight_lesions_medium().intValue() > 5 ){
			    dwmParietalScheltens = new Integer(4);
			} else if (getAssessment_regions_deepWhiteMatter_parietal().getLeft_lesions_medium().intValue() + getAssessment_regions_deepWhiteMatter_parietal().getRight_lesions_medium().intValue() > 0){
			    dwmParietalScheltens = new Integer(3);
			} else if (getAssessment_regions_deepWhiteMatter_parietal().getLeft_lesions_small().intValue() + getAssessment_regions_deepWhiteMatter_parietal().getRight_lesions_small().intValue() > 5){
			    dwmParietalScheltens = new Integer(2);
			} else if (getAssessment_regions_deepWhiteMatter_parietal().getLeft_lesions_small().intValue() + getAssessment_regions_deepWhiteMatter_parietal().getRight_lesions_small().intValue() > 0){
			    dwmParietalScheltens = new Integer(1);
			} else {	
			    dwmParietalScheltens = new Integer(0);
			}
		}

		return dwmParietalScheltens;
	}

	public Integer getScheltensDwmOccipital(){
		Integer dwmOccipitalScheltens = this.getAssessment_regions_deepWhiteMatter_occipital().getScheltens();
		if (dwmOccipitalScheltens == null){
		    if (this.getAssessment_regions_deepWhiteMatter_occipital().getLeft_confluent().booleanValue() || getAssessment_regions_deepWhiteMatter_occipital().getRight_confluent().booleanValue() ){
		        dwmOccipitalScheltens = new Integer(6);
			} else if (this.getAssessment_regions_deepWhiteMatter_occipital().getLeft_lesions_large().intValue() + getAssessment_regions_deepWhiteMatter_occipital().getRight_lesions_large().intValue() > 0){
			    dwmOccipitalScheltens = new Integer(5);
			} else if (getAssessment_regions_deepWhiteMatter_occipital().getLeft_lesions_medium().intValue() + getAssessment_regions_deepWhiteMatter_occipital().getRight_lesions_medium().intValue() > 5 ){
			    dwmOccipitalScheltens = new Integer(4);
			} else if (getAssessment_regions_deepWhiteMatter_occipital().getLeft_lesions_medium().intValue() + getAssessment_regions_deepWhiteMatter_occipital().getRight_lesions_medium().intValue() > 0){
			    dwmOccipitalScheltens = new Integer(3);
			} else if (getAssessment_regions_deepWhiteMatter_occipital().getLeft_lesions_small().intValue() + getAssessment_regions_deepWhiteMatter_occipital().getRight_lesions_small().intValue() > 5){
			    dwmOccipitalScheltens = new Integer(2);
			} else if (getAssessment_regions_deepWhiteMatter_occipital().getLeft_lesions_small().intValue() + getAssessment_regions_deepWhiteMatter_occipital().getRight_lesions_small().intValue() > 0){
			    dwmOccipitalScheltens = new Integer(1);
			} else {	
			    dwmOccipitalScheltens = new Integer(0);
			}
		}

		return dwmOccipitalScheltens;
	}

	public Integer getScheltensDwmTemporal(){
		Integer dwmTemporalScheltens = this.getAssessment_regions_deepWhiteMatter_temporal().getScheltens();
		if (dwmTemporalScheltens == null){
		    if (this.getAssessment_regions_deepWhiteMatter_temporal().getLeft_confluent().booleanValue() || getAssessment_regions_deepWhiteMatter_temporal().getRight_confluent().booleanValue() ){
		        dwmTemporalScheltens = new Integer(6);
			} else if (this.getAssessment_regions_deepWhiteMatter_temporal().getLeft_lesions_large().intValue() + getAssessment_regions_deepWhiteMatter_temporal().getRight_lesions_large().intValue() > 0){
			    dwmTemporalScheltens = new Integer(5);
			} else if (getAssessment_regions_deepWhiteMatter_temporal().getLeft_lesions_medium().intValue() + getAssessment_regions_deepWhiteMatter_temporal().getRight_lesions_medium().intValue() > 5 ){
			    dwmTemporalScheltens = new Integer(4);
			} else if (getAssessment_regions_deepWhiteMatter_temporal().getLeft_lesions_medium().intValue() + getAssessment_regions_deepWhiteMatter_temporal().getRight_lesions_medium().intValue() > 0){
			    dwmTemporalScheltens = new Integer(3);
			} else if (getAssessment_regions_deepWhiteMatter_temporal().getLeft_lesions_small().intValue() + getAssessment_regions_deepWhiteMatter_temporal().getRight_lesions_small().intValue() > 5){
			    dwmTemporalScheltens = new Integer(2);
			} else if (getAssessment_regions_deepWhiteMatter_temporal().getLeft_lesions_small().intValue() + getAssessment_regions_deepWhiteMatter_temporal().getRight_lesions_small().intValue() > 0){
			    dwmTemporalScheltens = new Integer(1);
			} else {	
			    dwmTemporalScheltens = new Integer(0);
			}
		}

		return dwmTemporalScheltens;
	}

	public Integer getScheltensPvFh(){
		Integer pvFhScheltens = this.getAssessment_regions_periventricular_frontalHorns().getScheltens();
		if (pvFhScheltens == null){
			if (this.getAssessment_regions_periventricular_frontalHorns().getLeft().intValue() > getAssessment_regions_periventricular_frontalHorns().getRight().intValue() ){
				pvFhScheltens = getAssessment_regions_periventricular_frontalHorns().getLeft() ;
			} else {
				pvFhScheltens = getAssessment_regions_periventricular_frontalHorns().getRight() ;
			}
		}

		return pvFhScheltens;
	}

	public Integer getScheltensPvPh(){
		Integer pvPhScheltens = super.getAssessment_regions_periventricular_posteriorHorns().getScheltens();
		if (pvPhScheltens == null){
			if (this.getAssessment_regions_periventricular_posteriorHorns().getLeft().intValue() > getAssessment_regions_periventricular_posteriorHorns().getRight().intValue() ){
			    pvPhScheltens = getAssessment_regions_periventricular_posteriorHorns().getLeft() ;
			} else {
			    pvPhScheltens = getAssessment_regions_periventricular_posteriorHorns().getRight() ;
			}
		}

		return pvPhScheltens;
	}

	public Integer getScheltensPvVb(){
		Integer pvVbScheltens = this.getAssessment_regions_periventricular_ventricularBody().getScheltens();
		if (pvVbScheltens == null){
		    if (this.getAssessment_regions_periventricular_ventricularBody().getLeft().intValue() > getAssessment_regions_periventricular_ventricularBody().getRight().intValue() ){
		        pvVbScheltens = getAssessment_regions_periventricular_ventricularBody().getLeft() ;
			} else {
			    pvVbScheltens = getAssessment_regions_periventricular_ventricularBody().getRight() ;
			}
		}

		return pvVbScheltens;
	}

	public Integer getScheltensBgThalamus(){
		Integer bgThalamusScheltens = this.getAssessment_regions_basalGanglia_thalamus().getScheltens();
		if (bgThalamusScheltens == null){
			if (this.getAssessment_regions_basalGanglia_thalamus().getLeft_confluent().booleanValue() || getAssessment_regions_basalGanglia_thalamus().getRight_confluent().booleanValue() ){
				bgThalamusScheltens = new Integer(6);
			} else if (this.getAssessment_regions_basalGanglia_thalamus().getLeft_lesions_large().intValue() + getAssessment_regions_basalGanglia_thalamus().getRight_lesions_large().intValue() > 0){
				bgThalamusScheltens = new Integer(5);
			} else if (getAssessment_regions_basalGanglia_thalamus().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_thalamus().getRight_lesions_medium().intValue() > 5 ){
				bgThalamusScheltens = new Integer(4);
			} else if (getAssessment_regions_basalGanglia_thalamus().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_thalamus().getRight_lesions_medium().intValue() > 0){
				bgThalamusScheltens = new Integer(3);
			} else if (getAssessment_regions_basalGanglia_thalamus().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_thalamus().getRight_lesions_small().intValue() > 5){
				bgThalamusScheltens = new Integer(2);
			} else if (getAssessment_regions_basalGanglia_thalamus().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_thalamus().getRight_lesions_small().intValue() > 0){
				bgThalamusScheltens = new Integer(1);
			} else {	
				bgThalamusScheltens = new Integer(0);
			}
		}

		return bgThalamusScheltens;
	}

	public Integer getScheltensBgCaudate(){
		Integer bgCaudateScheltens = super.getAssessment_regions_basalGanglia_caudate().getScheltens();
		if (bgCaudateScheltens == null){
		    if (this.getAssessment_regions_basalGanglia_caudate().getLeft_confluent().booleanValue() || getAssessment_regions_basalGanglia_caudate().getRight_confluent().booleanValue() ){
		        bgCaudateScheltens = new Integer(6);
			} else if (this.getAssessment_regions_basalGanglia_caudate().getLeft_lesions_large().intValue() + getAssessment_regions_basalGanglia_caudate().getRight_lesions_large().intValue() > 0){
			    bgCaudateScheltens = new Integer(5);
			} else if (getAssessment_regions_basalGanglia_caudate().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_caudate().getRight_lesions_medium().intValue() > 5 ){
			    bgCaudateScheltens = new Integer(4);
			} else if (getAssessment_regions_basalGanglia_caudate().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_caudate().getRight_lesions_medium().intValue() > 0){
			    bgCaudateScheltens = new Integer(3);
			} else if (getAssessment_regions_basalGanglia_caudate().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_caudate().getRight_lesions_small().intValue() > 5){
			    bgCaudateScheltens = new Integer(2);
			} else if (getAssessment_regions_basalGanglia_caudate().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_caudate().getRight_lesions_small().intValue() > 0){
			    bgCaudateScheltens = new Integer(1);
			} else {	
			    bgCaudateScheltens = new Integer(0);
			}
		}

		return bgCaudateScheltens;
	}

	public Integer getScheltensBgPutamen(){
		Integer bgPutamenScheltens = super.getAssessment_regions_basalGanglia_putamen().getScheltens();
		if (bgPutamenScheltens == null){
		    if (this.getAssessment_regions_basalGanglia_putamen().getLeft_confluent().booleanValue() || getAssessment_regions_basalGanglia_putamen().getRight_confluent().booleanValue() ){
		        bgPutamenScheltens = new Integer(6);
			} else if (this.getAssessment_regions_basalGanglia_putamen().getLeft_lesions_large().intValue() + getAssessment_regions_basalGanglia_putamen().getRight_lesions_large().intValue() > 0){
			    bgPutamenScheltens = new Integer(5);
			} else if (getAssessment_regions_basalGanglia_putamen().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_putamen().getRight_lesions_medium().intValue() > 5 ){
			    bgPutamenScheltens = new Integer(4);
			} else if (getAssessment_regions_basalGanglia_putamen().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_putamen().getRight_lesions_medium().intValue() > 0){
			    bgPutamenScheltens = new Integer(3);
			} else if (getAssessment_regions_basalGanglia_putamen().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_putamen().getRight_lesions_small().intValue() > 5){
			    bgPutamenScheltens = new Integer(2);
			} else if (getAssessment_regions_basalGanglia_putamen().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_putamen().getRight_lesions_small().intValue() > 0){
			    bgPutamenScheltens = new Integer(1);
			} else {	
			    bgPutamenScheltens = new Integer(0);
			}
		}

		return bgPutamenScheltens;
	}

	public Integer getScheltensBgGp(){
		Integer bgGpScheltens = this.getAssessment_regions_basalGanglia_globusPallidus().getScheltens();
		if (bgGpScheltens == null){
		    if (this.getAssessment_regions_basalGanglia_globusPallidus().getLeft_confluent().booleanValue() || getAssessment_regions_basalGanglia_globusPallidus().getRight_confluent().booleanValue() ){
		        bgGpScheltens = new Integer(6);
			} else if (this.getAssessment_regions_basalGanglia_globusPallidus().getLeft_lesions_large().intValue() + getAssessment_regions_basalGanglia_globusPallidus().getRight_lesions_large().intValue() > 0){
			    bgGpScheltens = new Integer(5);
			} else if (getAssessment_regions_basalGanglia_globusPallidus().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_globusPallidus().getRight_lesions_medium().intValue() > 5 ){
			    bgGpScheltens = new Integer(4);
			} else if (getAssessment_regions_basalGanglia_globusPallidus().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_globusPallidus().getRight_lesions_medium().intValue() > 0){
			    bgGpScheltens = new Integer(3);
			} else if (getAssessment_regions_basalGanglia_globusPallidus().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_globusPallidus().getRight_lesions_small().intValue() > 5){
			    bgGpScheltens = new Integer(2);
			} else if (getAssessment_regions_basalGanglia_globusPallidus().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_globusPallidus().getRight_lesions_small().intValue() > 0){
			    bgGpScheltens = new Integer(1);
			} else {	
			    bgGpScheltens = new Integer(0);
			}
		}

		return bgGpScheltens;
	}

	public Integer getScheltensBgIc(){
		Integer bgIcScheltens = this.getAssessment_regions_basalGanglia_internalCapsule().getScheltens();
		if (bgIcScheltens == null){
		    if (this.getAssessment_regions_basalGanglia_internalCapsule().getLeft_confluent().booleanValue() || getAssessment_regions_basalGanglia_internalCapsule().getRight_confluent().booleanValue() ){
		        bgIcScheltens = new Integer(6);
			} else if (this.getAssessment_regions_basalGanglia_internalCapsule().getLeft_lesions_large().intValue() + getAssessment_regions_basalGanglia_internalCapsule().getRight_lesions_large().intValue() > 0){
			    bgIcScheltens = new Integer(5);
			} else if (getAssessment_regions_basalGanglia_internalCapsule().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_internalCapsule().getRight_lesions_medium().intValue() > 5 ){
			    bgIcScheltens = new Integer(4);
			} else if (getAssessment_regions_basalGanglia_internalCapsule().getLeft_lesions_medium().intValue() + getAssessment_regions_basalGanglia_internalCapsule().getRight_lesions_medium().intValue() > 0){
			    bgIcScheltens = new Integer(3);
			} else if (getAssessment_regions_basalGanglia_internalCapsule().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_internalCapsule().getRight_lesions_small().intValue() > 5){
			    bgIcScheltens = new Integer(2);
			} else if (getAssessment_regions_basalGanglia_internalCapsule().getLeft_lesions_small().intValue() + getAssessment_regions_basalGanglia_internalCapsule().getRight_lesions_small().intValue() > 0){
			    bgIcScheltens = new Integer(1);
			} else {	
			    bgIcScheltens = new Integer(0);
			}
		}

		return bgIcScheltens;
	}
	
	public int getTotalScheltens()
	{
		return (this.getScheltensBgCaudate().intValue() + this.getScheltensBgGp().intValue() + this.getScheltensBgIc().intValue() + this.getScheltensBgPutamen().intValue() + this.getScheltensBgThalamus().intValue() + this.getScheltensDwmFrontal().intValue() + this.getScheltensDwmOccipital().intValue() + this.getScheltensDwmParietal().intValue() + this.getScheltensDwmTemporal().intValue() + this.getScheltensPvFh().intValue() + this.getScheltensPvPh().intValue() + this.getScheltensPvVb().intValue());
	}
	
	public int getDWMScheltens()
	{
		return (this.getScheltensDwmFrontal().intValue() + this.getScheltensDwmOccipital().intValue() + this.getScheltensDwmParietal().intValue() + this.getScheltensDwmTemporal().intValue());
	}
	
	public int getPVScheltens()
	{
		return (this.getScheltensPvFh().intValue() + this.getScheltensPvPh().intValue() + this.getScheltensPvVb().intValue());
	}
	
	public int getBGScheltens()
	{
		return (this.getScheltensBgCaudate().intValue() + this.getScheltensBgGp().intValue() + this.getScheltensBgIc().intValue() + this.getScheltensBgPutamen().intValue() + this.getScheltensBgThalamus().intValue());
	}
	
	public int getDWMBurden()
	{
		return (this.getDWMScheltens() + this.getPVScheltens());
	}
	
	public int getFrontalVolumeTotal()
	{
		return (this.getAssessment_regions_deepWhiteMatter_frontal().getLeft_volume().intValue() + this.getAssessment_regions_deepWhiteMatter_frontal().getRight_volume().intValue());
	}
	
	public int getOccipitalVolumeTotal()
	{
		return (this.getAssessment_regions_deepWhiteMatter_occipital().getLeft_volume().intValue() + this.getAssessment_regions_deepWhiteMatter_occipital().getRight_volume().intValue());
	}

	public int getParietalVolumeTotal()
	{
		return (this.getAssessment_regions_deepWhiteMatter_parietal().getLeft_volume().intValue() + this.getAssessment_regions_deepWhiteMatter_parietal().getRight_volume().intValue());
	}
	
	public int getTemporalVolumeTotal()
	{
		return (this.getAssessment_regions_deepWhiteMatter_temporal().getLeft_volume().intValue() + this.getAssessment_regions_deepWhiteMatter_temporal().getRight_volume().intValue());
	}
}
