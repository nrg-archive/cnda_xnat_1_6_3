##Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
#set ($template = $data.getTemplateInfo())
$!template.setLayoutTemplate("/Popup.vm")

#set ($sessionId = $om.getLabel())

<!-- get Javascript Dependencies -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="/scripts/xModal/xModal.js"></script>

<link href="/scripts/xModal/xModal.css" rel="stylesheet" />
<style type="text/css">
    body { margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 100%; background-color: #f3f3f3; }
    .hidden { display: none; }
    .col-2 { width: 45%; float:left; padding-right: 5%; }

    .xmodal div.error { margin-top: 0; }

    .xmodal fieldset { border-color: #ccc; border-style: solid; border-width: 1px 0 0; margin: 2em 0; padding-top: 10px; }
    .xmodal fieldset legend { border-radius: 3px; padding: 3px; background-color: inherit; }
    .xmodal fieldset label { display: inline-block; font-weight: bold; min-width: 120px; padding-right: 10px;  }
    .xmodal fieldset .col-2 label { min-width: 75px; }

    .xmodal fieldset.error { background-color: #fffcc7; }
    .xmodal fieldset.error legend { color: #c33; }
    .xmodal fieldset.error .required { border-color: #c33; }

    .xmodal input, .xmodal select, .xmodal textarea { border: 1px solid #999; padding: 2px 1px; }

    .xmodal table { border: 1px solid #ccc; }
    .xmodal td, .inputWrapper th { padding: 3px; }
    .xmodal td { border-top: 1px solid #ccc; }
    .xmodal th { text-align: left; background-color: #f3f3f3; }
    tr.empty { background-color: #fffcc7; color: #933; }

    div.xmodal .footer { position: fixed; bottom: 0; right: 0; width: inherit; }
    div.xmodal .footer .buttons .textlink { font-size: 12px; padding: 0 8px; position: relative; top: 5px; }
    div.xmodal.embedded { top: 0; left: 0; border: none; box-shadow: none; display: block; z-index: auto;  }
</style>

<script language="Javascript">

    var scansToBuild = new Array();

    function disableSubmit() {
        document.getElementById("submitForm").disabled = true;
    }


    function enableSubmit() {
        document.getElementById("submitForm").disabled = false;
    }

    function disableNext() {
        $("#button-advance").addClass("disabled");
    }

    function enableNext() {
        $("#button-advance").removeClass("disabled");
    }


    function validate() {
        var checkedScans = $("input[name^=scan]:checked");
        var ret = checkedScans && checkedScans.length > 0
        if (ret) {
            enableNext();
        } else {
            alert("At least one scan must be selected.");
            disableSubmit();
            disableNext();
        }

        return ret
    }

    function noneClicked(noneObj) {
        var type = noneObj.getAttribute("class")
        if (noneObj.checked) {
            var checkedScans = $("input[name^="+type+"]:checked");
            var l = checkedScans.length;
            for (var i = 0; i < l; i++) {
                $(checkedScans[i]).prop("checked", false)
            }
        }
    }

    function scanClicked(scanObj) {
        var type = scanObj.getAttribute("class")
        if (scanObj.checked) {
            $("#x"+type).prop("checked", false)
        } else {
            var checkedScans = $("input[name^="+type+"]:checked");
            if (checkedScans && checkedScans.length > 0) {
                $("#x"+type).prop("checked", "checked")
            }
        }
    }

    /* page config */

    // set popup window title properly
    $(document).prop("title","CNDA -- Configure FreeSurfer Pipeline for $sessionId");

    $(document).ready(function(){
        // set display height and width according to popup size
        $("#pipeline_modal").width($(window).width());
        $("#pipeline_modal").height($(window).height());
        $("#pipeline_modal .body").height($(window).height()-$("#pipeline_modal .footer").height());

        // hijack the form submit button and use the xmodal footer submit button instead.
        $("#button-advance").on("click",function(){
            if (confirm ("Build pipeline with these parameters?")) {

                // Make sure that if no scans of an optional type are selected, that "None" is selected.
                for (var type in ["flair","t2"]) {
                    var none = $("#x"+type)
                    if (none && !none.checked) {
                        // This type of scan exists, and the user has not selected "None"
                        // Are any of the scans selected?
                        var checkedScans = $("input[name^="+type+"]:checked");
                        if (!checkedScans || checkedScans.length == 0) {
                            // No, none of them are selected
                            none.prop("checked","checked")
                        }
                    }
                }

                // Submit the form
                $("#submitForm").click();
            }
        });
    });

    // toggle visibility of advanced parameters. (parameter values are always passed to form, whether or not they are visible)
    // $("#advanced-params-toggle").on("click",function(){
    //     var adv = $("#advanced-params");
    //     if ($(adv).hasClass("hidden")) {
    //         $(this).html("Hide");
    //         $(adv).removeClass("hidden");
    //     } else {
    //         $(this).html("Show");
    //         $(adv).addClass("hidden");
    //     }
    // });
    function advanced_params_toggle(){
        var adv = $("#advanced-params");
        if ($(adv).hasClass("hidden")) {
            $(this).html("Hide");
            $(adv).removeClass("hidden");
        } else {
            $(this).html("Show");
            $(adv).addClass("hidden");
        }
    }


</script>

<!-- create an xmodal container inside the popup window -->
<div id="pipeline_modal" class="xmodal fixed embedded">
    <div class="body content scroll">
        <!-- handle error messages -->
        #if ($data.message)
        <div class="error">$data.message</div>
        #end

        <div class="inner">
        <!-- modal body start -->

        <form name="FreesurferBuildOptions" method="post" action="$link.setAction("BuildAction")" onsubmit="return validate();" class="optOutOfXnatDefaultFormValidation">
            <h2>Set FreeSurfer Execution Parameters for $sessionId</h2>


            ## determine whether to take "easy button" path
            #set ($easyFS = false)
            #if ($projectId.startsWith("DIAN"))
                #set ($easyFS = true)
            #elseif ($projectId.startsWith("TIP"))
                #set ($easyFS = true)
            #end

            #if ($projectParameters.get("use_manual_qc").getCsvvalues()=="1")
            <!-- display and create an input for the preselected "best" scan -->
                #set ($j = 0)
                #set ($bestScanError = true)
                <fieldset>
                    <legend>Looking for Predetermined "Best Scan"</legend>
                    <p><em>Scan selection is automated for this project, based on the results of Manual QC.</em></p>
                    #foreach ($scan in $mrScans)
                        #if ($scan.getId().equals("$bestScan"))
                            <p><label><input type="checkbox" name="scan_$j" id="scan_$j" checked value="$scan.Id">$scan.Id ($!scan.getType() &mdash; Quality: $scan.Quality &mdash; QC: $projectParameters.get("qc_rating_scale").getCsvvalues() = $projectParameters.get("qc_rating").getCsvvalues())</label></p>
                            #set ($bestScanError = false)
                        #else
                            <p><label><input type="checkbox" name="scan_$j" id="scan_$j" value="$scan.Id">$scan.Id ($!scan.getType() &mdash; Quality: $scan.Quality)</label></p>
                        #end
                        #set ($j = $j + 1)
                    #end
                </fieldset>
                <input type="hidden" name="scan_rowcount" id="scan_rowcount" value="$j">

                #if ($bestScanError==true)
                    <script>
                        disableNext();
                    </script>
                    <fieldset class="error">
                        <legend>Looking for Predetermined "Best Scan"</legend>
                        <p><strong>ERROR: Could not select scan for pipeline. Please run (or re-run) Manual QC and determine a "Best Scan" for this session.</strong></p>
                    </fieldset>
                #end

            #else
            <!-- get users to determine which scan to use. Ignore any unusable scans -->
                <fieldset>
                    <legend>Determine best scan to use</legend>

                    #set ($j = 0)
                    #foreach ($scan in $mrScans)
                        #if ($scan.getQuality().equalsIgnoreCase("usable"))
                            #set ($attrib="checked")
                        #else
                            #set ($attrib="")
                        #end
                        <p><label><input type="checkbox" name="scan_$j" id="scan_$j" value="$scan.Id" $attrib onclick="validate()">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>

                        #set ($j = $j + 1)
                    #end
                </fieldset>
                <input type="hidden" name="scan_rowcount" id="scan_rowcount" value="$j">

            #end

            #if (!$t2Scans.isEmpty())
            <fieldset>
                <legend>Select T2 scan</legend>

                <p><label><input type="checkbox" class="t2" name="xt2" id="xt2" checked onclick="noneClicked(this)">None</label></p>
                #set ($j = 0)
                #foreach ($scan in $t2Scans)
                    <p><label><input type="checkbox" class="t2" name="t2_$j" id="t2_$j" value="$scan.Id" onclick="scanClicked(this)">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>

                    #set ($j = $j + 1)
                #end
            </fieldset>
            <input type="hidden" name="t2_rowcount" id="t2_rowcount" value="$j">
            #end

            #if (!$flairScans.isEmpty())
            <fieldset>
                <legend>Select FLAIR scan</legend>

                <p><label><input type="checkbox" class="flair" name="xflair" id="xflair" checked onclick="noneClicked(this)">None</label></p>
                #set ($j = 0)
                #foreach ($scan in $flairScans)
                    <p><label><input type="checkbox" class="flair" name="flair_$j" id="flair_$j" value="$scan.Id" onclick="scanClicked(this)">$scan.Id (<b>$!scan.getType()</b> Quality: $scan.Quality)</label></p>

                    #set ($j = $j + 1)
                #end
            </fieldset>
            <input type="hidden" name="flair_rowcount" id="flair_rowcount" value="$j">
            #end

            <fieldset style="background-color: #def">
                <legend>Advanced Parameters (<a href="javascript:" id="advanced-params-toggle" onclick="advanced_params_toggle()">Show</a>)</legend>
                <div class="hidden" id="advanced-params">
                    <p><label for="recon_all_args" title="Arguments to pass to recon-all">recon-all args</label>
                    #set($recon_all_args=$projectParameters.get("recon_all_args").csvvalues)
                    <input type="text" name="recon_all_args" id="recon_all_args" value="$recon_all_args" size="75" /></p>

                ##  Figure out how to pick resources here.
                ##  Use javascript on scan checkbox onclick.
                ##  If all checked have dicom and don't have nifti, format=DICOM
                ##  If all checked have nifti and don't have dicom, format=NIFTI
                ##  else radio buttom: (x) DICOM or ( ) NIFTI
                    <input type="hidden" name="format" id="format" value="DICOM"/>
                    <fieldset>
                        <legend>Email Notification List</legend>
                        #pipelineEmailNotif("${pipeline.getName()}-emails.lst" "")
                    </fieldset>
                </div>
            <p></p>

            </fieldset>

            #xdatPassItemFormFields($search_element $search_field $search_value $project)
            ## <input type="hidden" name="isDicom" value="$isDicom">
            <input type="hidden" name="pipeline_location" value="$pipeline.getLocation()"/>
            <input type="hidden" name="cmdprefix" value="arc-grid-queue"/>
            <div class="hidden">
                <input type="submit" id="submitForm" name="eventSubmit_doFreesurferfivethree" value="Queue Job" />
            </div>
        </form>

        <!-- modal body end -->
        </div>
    </div>
    <div class="footer" style="background-color: rgb(240, 240, 240); border-color: rgb(224, 224, 224);">
        <!-- multi-panel controls are placed in footer -->
        <div class="inner">
            <span class="buttons">
                <a class="cancel close textlink" href="javascript:self.close()">Cancel</a>
                <a id="button-advance" class="default button panel-toggle" href="javascript:">Submit Pipeline</a>
            </span>
        </div>
    </div>
</div>
<!-- end of xmodal container -->
